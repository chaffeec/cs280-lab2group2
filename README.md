# DataGenerator Tutorial

1. Navigate to the ```../code/``` directory.

    ![Inside /code/ Directory](images/tutorial1.png "item1")

2. If you wish to provide a list of objects using a separate text file, follow these steps; otherwise skip to step 3:

    a. Open the "input.txt" file in the ```../code/``` directory

    b. Type the name of each item you wish to include in the original list, separating each item with a new line (i.e. press the Enter key after typing each item).

        ![Sample input.txt File](images/tutorial3.png "item3")

    c. Save the file.
    d. Open a terminal window (for Linux OS) or command prompt (for Windows OS) in the current ```../code/``` directory.
    e. Type the following command in the window:

         ```java -cp bin DataGenerator```

3. If you wish to provide a list of objects using the console, follow these steps; otherwise skip to step 4:

    a. Open a terminal window (for Linux OS) or command prompt (for Windows OS) in the current ```../code/``` directory.

    b.  Type the following command in the window:

         ```java -cp bin DataGenerator "item1;item2;item3"```

         Using this format you can include any number of objects, using a semicolon (;) to separate each one.  NOTE: Be sure to include quotation marks ("") around the list as shown.
    c.  For example, you might type:
        
        ```java -cp bin DataGenerator "1;2;3;4"```

        or

        ```java -cp bin DataGenerator "one;two;three;four"```

4. Output will be displayed in the terminal window, showing each of the different lists that can be generated from the original list.

    ![Sample Console Output](images/tutorial4.png "item4")

5. Output will also be sent to the "output.txt" file, again located in the same /code/ directory.

    ![Sample output.txt File](images/tutorial5.png "item5")
