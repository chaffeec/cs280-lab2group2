# Description of Roles

## Cathal: Tester (Team 1) & Ayodele: Tester (Team 2)
Cathal was responsible for writing the JUnit test cases for Team 1, while Ayodele was responsible for writing the JUnit test cases for Team 2.  Both members needed to review the JUnit software prior to use and then develop comprehensive ways to test each system using this tool.  They further wrote tutorials for their particular test cases, such that anyone would be able to run them easily and without error.

## Michael: Maintainer and Documenter
Michael was initially responsible for communicating with Team 1's maintainer, Almog, to understand the similarities and differences between the systems created by both teams.   He then devised an activity graph to outline the milestones and activities needed for the completion of the project.  He proceeded to write evaluations for both teams' systems, and then began to update most of the previously completed deliverables (requirements analysis, design document, tutorial, and implementation) upon consulting with the rest of team.