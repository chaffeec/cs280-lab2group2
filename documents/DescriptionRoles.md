# Team 2:

* Michael Camara
* Cathal Chaffeec
* Ayodele Hamilton

+ Requirements Analyst: Ayodele.  
The requirements analyst job was to conference on the customer to find out the specifications of the program that needed to be implemented. The customer tells the analyst what is needed for the project to run and how it should run. The analyst needs to know what the project will be used for in the long run and how it needs to be implemented when the first part of the program is complete. Then as a group we had to figure out how to make the program run to fit the specifications of the customer.

+ Designer: Cathal.  
Draws out a way to implement the program based on what is said by the customer. The chart is drawn out to map out the different Java classes, the text files, the main class, the method and the algorithms used to make the program function.

+ Programming and Testing: Michael.  
Implements the designed based on what was said by the requirements analyst and the designer. Follows the chart to create the program and designs fitted for the customer. Then after the program is implemented, then it will be tested using debuggers. The debuggers are there to make sure that there arent anything that can jeopardized the performance and cause duplication of some kind that should not be there.

+ Documenter and Trainer: Ayodele.  
The documenter and trainer's job is to get the program in writing about the behavior what is there for the customer and explanations on the different features of the program. The trainer then shows the customer how to run the program and shows them how to navigate throughout the program to see if the program is what was asked for.

+ Maintainer(s):  Michael.  
The maintainers keep the program running due to upgrades, bug fixes and keeping up with the customer needs and wants.
