#Tutorial for Team 2 Test Case by Team 2

##Test Name: TestDataGenerator.java 

This test case checks if the permutations for the program are correct with the given size of the list. 

##To Run the TestDataGenerator: 

1.  Set Classpath: export CLASSPATH=/.../lib/junit-4.12.jar:../src/TestDataGenerator.java 

2.  To Compile: javac TestDataGenerator.java 

3.  To Run:java TestDataGenerator 

4.  The Output 

    The output reads true if the number of permutations agree with the size of the list. If it doesn't agree then the test will read back false. 
 
![Example console output](../images/test_tutorial_image.png)
