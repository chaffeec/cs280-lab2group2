# Requirements Analysis:

## Summary:  

The customer is seeking a data generating program that will be incorporated into a bigger system for the company he works for. The program will be used for an enterprise data management system, which will help manage the data files received by the company. The specific purpose of the program is to generate a list of objects that stem from an original list obtained from the user, using a particular algorithm described by the customer (explained later in the "*Behavior*" section). The customer wants the program to be user friendly such that everybody in the company will be able to use it. The program should be easy to update and the company should be able to work with any new changes that are implemented.   The original list used by the program can be entered in a console window or included in an external file.  Similarly the output should appear in both the console and also in a separate file.

## Inputs:  

The customer should be able to provide input for the program either directly through the console, or externally through a plain text file.  The content of the input should be a list of objects.  If input is given through the console, the list of objects should be delimited by a semicolon (";").  If input is given through an external text file, the list of objects should be delimited by newline characters (i.e. each line of the file corresponds to a separate object).  

## Behavior:  

The customer wants to use a particular algorithm to generate new lists based on the original list obtained from the input.  Starting with the first index from the original list, the algorithm will generate a new list by swapping that first index with every index in front of it.  These indices are always picked from the original list, *not* from the generated lists obtained via swapping.  After swapping with the last index of the list, the swapping resets, this time starting with the second index in the original list and swapping with every index in front of it.  This continues until the last two indices are swapped, and then the algorithm finishes, generating a total of *__n(n-1)/2__* new lists.  See the following example:

```
L  = {1, 2, 3, 4}	// original list
L1 = {2, 1 ,3, 4}	// swapped index 0 and 1 from original list
L2 = {3, 2, 1, 4}	// swapped index 0 and 2 from original list
L3 = {4, 2, 3, 1}	// swapped index 0 and 3 from original list
L4 = {1, 3, 2, 4}	// swapped index 1 and 2 from original list
L5 = {1, 4, 3, 2}	// swapped index 1 and 3 from original list
L6 = {1, 2, 4, 3}	// swapped index 2 and 3 from original list
```

## Outputs:  

The output of the program will show all lists that have variations of the first list using the previously described algorithm.  For an original list containing four items, the customer requests the specific format of: *L# = {item1, item2, item3, item4}*, where "#" is simply the order in which the new lists have been generated. The output should be displayed in the console window, and it should also be sent to an external plain text file for alternative access.  
