Design
====================

## Classes
* Input.java 
* DataGenerator.java
* Sorter.java
* Output.java

---

## Description

The DataGenerator class will contain the ```main()``` method and control the sequence of steps in the program, advancing from the Input class, to the Sorter class, and finally to the Output class.

The Input class will import a list of objects either through a text document labeled "input.txt" in the ```~/code/``` directory via the ```importFile()``` method, or directly through the console via the ```importText()``` method.  The method of input will be determined based on whether the user has entered command line arguments upon program execution: if no arguments are used, then ```importFile()``` is selected by default, otherwise ```importText()``` is selected.  Regardless of the method used, this class will parse the list and create an ```ArrayList``` containing each of the objects.

The DataGenerator class will then send this list as an argument to the ```sort()``` method of the Sorter class.  This method will take the original list of objects and do the appropriate swapping to find different permutations of the original data, following the algorithm specified by the customer in the requirements analysis document.  Each permutation of objects will be saved as an ```ArrayList``` and be put into an ```ArrayList``` of ```ArrayLists```, which we will call the ```permutationList``` for clarity.  Once all valid permutations have been created, the ```permutationList``` will be passed back to DataGenerator.

Finally, the ```permutationList``` will be sent to the Output class via separate calls to ```exportFile()``` and ```exportText()```.  The ```exportFile()``` method will print all the permutations of data into a text document labeled "output.txt."  The ```exportText()``` method will instead print all the permutations to the console for immediate viewing.

---

# Diagram

![Class Diagram](../images/design_diagram.pdf "Class Diagram")
