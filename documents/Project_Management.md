#Project Management

Our team followed a relatively simple plan for completing Lab 3.  We first delegated roles, assigning Ayodele and Cathal to produce JUnit test cases while Michael handled the documentation and inter-team communication.  This arrangement allowed all team members to experience different roles and responsibilities compared to the previous lab.  Michael further created an activity graph to outline milestones and activities that each team member would be responsible for.  This helped provide guidance for everyone and ensured that all tasks could be completed within one week.

![Activity Graph with Milestones (Nodes) and Activities (Edges)](Activity_Graph.pdf)
