# Evaulation of Team 2's Lab 2

+ Gary Miller
+ Almog Boanos
+ cody Kinneer
+ elizabeth Person

Team 2 produced a very thorough requirement analysis document. The team a very systematic approach by outlining first the overall summary of the assignment, the inputs, the outputs, and the behavvior of the system. The summary gives a good explantion of the goals of the project, also the inputs and outputs sections makes clear what the system needs to operate properly and what exactly the system will produce. Finally, the behavior section outlines the approach in which the team planned on taking when building the system. The next document that the team produced was a description of everyone's roles. This once again is very thorough and gives a strong sense of who on the team will be completing each task. These documents are both very straightforward and demonstrate the ideas and plan of action taken by the group very well.

The design documentation created by the team is also easy to understand and read. It clearly lists out the four classes that will be used to build the small system, followed by a short description. This desciption is also accompanied by a visual representation of the design of the system. the fact that the team produced the design plan in both writing and visually makes it very easy to visualize and understand the concept of how the system will be set up and interact among classes.

The tutorial document makes it very easy to understand how the system is actually ran. The team's document makes good use of instructional statements accompanied by visual aid to demonstrate the steps required to run the system. To further support the quality of this document, errors occurred because it was incorrectly ran. After a quick read of the tutorial documentation, only about a minute in length, it became clear how to correctly run the system.

Overall the system is designed and implemneted in a very user-friendly and efficient manner. One good aspect of the system is the how easy it is to change the input list given to the system. the fact that the inputs are generated from a text file, it seems that this will work well when being integrated into a greater system. Going alongside that, it is very that the output is also saved into a text file. as stated before this should make it quite easy to integrate into a larger system.
