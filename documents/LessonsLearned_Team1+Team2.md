# Lessons Learned

| Team 1           | Team 2           |
|------------------|------------------|
| Almog Boanos     | Michael Camara   |
| Cody Kinneer     | Cathal Chaffee   |
| Elizabeth Person | Ayodele Hamilton |
| Gary Miller      |                  |

## Requirements Analysis
1.  The importance of patience when interacting with the customer and being able to re-word questions to get the answers that we developers need.
2.  The difficulties of explaining specific implementation details to customers who do not have the same background in computer science.
3.  Ensuring that the documentation is exactly what the customer and team members need will greatly help everyone involved in the project.

## Design
1.  The importance of knowing the requirements from the customer as soon as possible in order to implement a functional design that the implementor can then work with.
2.  Designing before knowing the specific requirements can lead to wasted time during implementation.
3.  The design visualizations should clearly show the relationship between classes and methods.
4.  Managing the trade-off between designing too early and waiting too long to get started, i.e. managing slack time effectively, can be very tricky.

## Implementation
1.  Implementation should be performed with testability in mind.
2.  The importance of getting full details about the customer's specific request before writing large portions of the system.
3.  The difficulties associated with understanding the different parts of the assignment obtained from both the designer and the requirements analyst.
4.  The importance of communicating with team members if certain implementation details may require altering the design or clarifying customer requirements.

## Testing
1.  It's important to design the software with testing in mind, as it will make testing a lot easier and save significant time.
2.  Having previous testing experience is very useful, since for someone who is just starting to work with JUnit it can be very daunting.
3.  The importance of understanding how the partner team's code works and how to run it properly.

## Project management
1.  A good final product comes from frequent team communication, having a good overall game plan in mind, properly delegating tasks and working diligently within our time limit.