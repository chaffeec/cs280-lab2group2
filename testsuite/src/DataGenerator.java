import java.util.ArrayList;
import java.util.List;

/**
 * The DataGenerator class begins the program execution.  It includes importing a list of items from
 * "input.txt" in the current directory (via Input), generating all possible permutations of that list
 * using a simple algorithm (via Sorter), and sending output to the console and to "output.txt" (via Output).
 * It further includes a simple test to check for duplicated entries and to ensure that the proper
 * number of permutations has been created by the Sorter class.
 *
 * @author Team 2: Michael Camara, Cathal Chaffee, Ayodele Hamilton
 *
 */
public class DataGenerator {
private int pass=0;
private int fail=0;


	public void team2(String s, int times) {

		// Input the initial list of items from "input.txt"
		ArrayList<String> initialItems = Input.importFile(s);

		// Create sorter for generating permutations
		Sorter sorter = new Sorter();

		// Use sorter to generate list of possible permutations
		ArrayList<ArrayList<String>> permutationList = sorter.sort(initialItems);

		Output.exportFile(permutationList);

		//////////////////////////////////////////////////////////
		//	Test for duplicate entries in the permutation list
		//////////////////////////////////////////////////////////

		// Iterate through all permutations
		int numDuplicates = 0;
		for(int i = 0; i < permutationList.size() - 1; i++) {

			// Store a single permutation
			ArrayList<String> targetPermutation = permutationList.get(i);

			// Create a subset of the permutationList, excluding the targetPermutation
			List<ArrayList<String>> subsetList = permutationList.subList(i + 1, permutationList.size() - 1);

			// Check if the subset contains the targetPermutation
			// If yes, then a duplicate was found
			if(subsetList.contains(targetPermutation)) {
				//System.out.println("Duplicate: " + targetPermutation);
				numDuplicates++;
			}
		}

		//System.out.println("Number of duplicates: " + numDuplicates);

		//////////////////////////////////////////////////////////
		//	Test for number of permutations created relative to expected number of permutations
		//////////////////////////////////////////////////////////

		// Number of unique items in the initial list of items
		int n = initialItems.size();

		int expectedPermutations = 0;
		int actualPermutations = permutationList.size();

		// Calculate the number of expected permutations given the desired algorithm

			expectedPermutations = ((times*(times-1))/2);


		//System.out.println("Expected number of permutations: " + expectedPermutations);
		//System.out.println("Actual number of permutations: " + actualPermutations);

		//	Indicate whether the results have passed these tests
		if(expectedPermutations == actualPermutations) {
			pass++;
			//System.out.println("Test Status:  PASSED");

		}
		else {
			System.out.println("Test Status:  FAILED");
			fail++;
            System.out.println("Failure!!!!. Test has failed at case number"+writer.k);
		}
	}

	  public Integer getPass() {
	      return pass;
	    }
	  public Integer getFailed() {
	      return fail;
	    }

}

