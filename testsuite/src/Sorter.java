import java.util.ArrayList;

/** This class uses a simple sorting algorithm to create a subset of permutations
 * given an original list of items.
 * 
 * @author Team 2: Michael Camara, Cathal Chaffee, Ayodele Hamilton
 *
 */
public class Sorter {
	
	public ArrayList<ArrayList<String>> sort(ArrayList<String> initialItems) {

		// List containing all of the permutations for the original list, given the requested algorithm
		ArrayList<ArrayList<String>> permutationList = new ArrayList<ArrayList<String>>();

		// Iterate through the original list of items
		for(int startIndex = 0; startIndex < initialItems.size(); startIndex++) {

			// Iterate through all indices greater than the current startIndex
			for(int endIndex = startIndex + 1; endIndex < initialItems.size(); endIndex++) {

				// Create a duplicate of the original list
				ArrayList<String> updatedList = new ArrayList<String>(initialItems);

				// Swap the items from the start and end indices
				String firstItem = updatedList.get(startIndex);
				String secondItem = updatedList.get(endIndex);
				updatedList.set(startIndex, secondItem);
				updatedList.set(endIndex, firstItem);

				// Add this adjusted list to the list of possible permutations
				permutationList.add(new ArrayList<String>(updatedList));

			}
		}

		return permutationList;
	}
}
