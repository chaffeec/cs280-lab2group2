import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/** This class controls how the results of the sorting algorithm are displayed to
 * the user.  By default, the list of permutations are both displayed in the
 * console and exported to a file named "output.txt."  
 * 
 * @author Team 2: Michael Camara, Cathal Chaffee, Ayodele Hamilton
 *
 */
public class Output {

	static void exportFile(ArrayList<ArrayList<String>> permutationList) {
		
		// Iterate through all permutations and print them to the console
		for(int i = 0; i < permutationList.size(); i++) {
			
			ArrayList<String> currentPermutation = permutationList.get(i);
			
			//System.out.print("L" + (i+1) + " = {");
			
			for(int j = 0; j < currentPermutation.size(); j++) {
				
				if(j != 0){
					//System.out.print(", ");
				//System.out.print(currentPermutation.get(j));
				}
				
			}
			
			//System.out.println("}");			
		}
		
		// Output the list of all permutations to a text file, called "output.txt" by default
		try {
			FileWriter writer = new FileWriter("output.txt", false);
			
			// Iterate through all permutations
			for(int i = 0; i < permutationList.size(); i++) {
				
				ArrayList<String> currentPermutation = permutationList.get(i);
				
				writer.write("L" + (i+1) + " = {");
				
				for(int j = 0; j < currentPermutation.size(); j++) {
				
					if(j != 0)
						writer.write(", ");
					writer.write(currentPermutation.get(j));
				}

				writer.write("}");
				
				// Separate permutations with a newline (except for final permutation)
				if(i != permutationList.size() - 1) {
					writer.write(System.getProperty("line.separator"));
				}

			}
			writer.close();
		} catch (IOException e) {
			System.out.println("Could not generate output file in current directory");
			e.printStackTrace();
		}		
	}
}