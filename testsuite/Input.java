import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/** This class controls how input is retrieved from the user.  By default,
 * the original list of items is obtained from the "input.txt" file.  Items
 * are delimited by carriage returns, such that every line in the .txt file
 * corresponds to a separate item.
 * 
 * @author Team 2: Michael Camara, Cathal Chaffee, Ayodele Hamilton
 *
 */
public class Input {
	static ArrayList<String> importFile(String s) {

		// Indicate file from which list of items will be obtained
		File importFile = new File(s);

		// Create a list to contain initial items 
		ArrayList<String> initialItems = new ArrayList<String>();

		// Obtain input from input.txt text file
		try {
			Scanner scan = new Scanner(importFile);

			// Scan input.txt line-by-line and add to initialItems
			// NOTE: Each line is considered its own String object
			while(scan.hasNext()) {
				String item = scan.nextLine();
				initialItems.add(item);
			}

//			System.out.println("Initial list of items: " + initialItems);
			scan.close();
		}

		// Throw error if the importFile is not found in the current directory
		catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Cannot find the " + importFile + " file in this directory");
			return null;
		}
		
		return initialItems;
		
	}
}
