/*Team 2 Lab 3: Test Case

Authors:Ayodele Hamilton,Michael Camara, and Cathal Chaffeec

JUnit.org: JUnit Test suites example
http://www.tutorialspoint.com/junit/junit_suite_test.htm
The format for the test cases comes the website above

Test Cases for Team 2
Set CLASSPATH to make sure it runs
*/


import static org.junit.Assert.*;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.*;
import org.junit.Test;

import java.util.*;
import java.lang.Object;
import java.util.Random;


public class TestDataGenerator
{
    public static void main(String[] args) {
        Result result = JUnitCore.runClasses(TestDataGenerator.class);
        for(Failure failure: result.getFailures()) {
            System.out.println(failure.toString());
        }
        System.out.println(result.wasSuccessful());
    }



    @Test   //Test the list for incorrect inputs
    public void testOne() {
		ArrayList<String> initialItems = new ArrayList<String>();
        initialItems.add("1");
        initialItems.add("2");
        initialItems.add("3");
        initialItems.add("4");
        System.out.println("Checking the size of the list: ");
		Sorter sorter = new Sorter();
		ArrayList<ArrayList<String>> permutationList = sorter.sort(initialItems);
        int expected = permutationList.size();
        for(int i = 0; i <= permutationList.size(); i++) {
            Random rand = new Random();
            ArrayList<Integer> list = new ArrayList<Integer>(10);
            for(int a = 0; a < 10; a++) {
                list.add(a);
            }
            int n = initialItems.size();
            int permutation, result;
            permutation = n * (n-1);
            result = permutation / 2;
            int actuals = result;
            assertEquals(expected, actuals);
        }
    }
}



