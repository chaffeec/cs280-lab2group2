import java.util.ArrayList;

/** This class uses a simple sorting algorithm to create a subset of permutations
 * given an original list of items.
 *
 * Description of algorithm from requirements document:
 *
 * Starting with the first index from the original list, the algorithm will generate
 * a new list by swapping that first index with every index in front of it.  These 
 * indices are always picked from the original list, *not* from the generated lists
 * obtained via swapping.  After swapping with the last index of the list, the swapping
 * resets, this time starting with the second index in the original list and swapping
 * with every index in front of it.  This continues until the last two indices are
 * swapped, and then the algorithm finishes, generating a total of n(n-1)/2 new lists.
 * 
 * @author Team 2: Michael Camara, Cathal Chaffee, Ayodele Hamilton
 */

public class Sorter {

    public ArrayList<ArrayList<String>> sort(ArrayList<String> initialItems) {

        // List containing all of the permutations for the original list, given the requested algorithm
        ArrayList<ArrayList<String>> permutationList = new ArrayList<ArrayList<String>>();

        // Iterate through the original list of items
        for(int startIndex = 0; startIndex < initialItems.size(); startIndex++) {

            // Iterate through all indices greater than the current startIndex
            for(int endIndex = startIndex + 1; endIndex < initialItems.size(); endIndex++) {

                // Create a duplicate of the original list
                ArrayList<String> updatedList = new ArrayList<String>(initialItems);

                // Swap the items from the start and end indices
                String firstItem = updatedList.get(startIndex);
                String secondItem = updatedList.get(endIndex);
                updatedList.set(startIndex, secondItem);
                updatedList.set(endIndex, firstItem);

                // Add this adjusted list to the list of possible permutations
                permutationList.add(new ArrayList<String>(updatedList));

            }
        }

        return permutationList;
    }
}
