import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/** This class controls how input is retrieved from the user.  By default,
 * the original list of items is obtained from the "input.txt" file via importFile().
 * Items from "input.txt" are delimited by carriage returns, such that every line in 
 * the file corresponds to a separate item.  Alternatively, the user can enter a list
 * of items via command line arguments, and they will be parsed using importText(). 
 * When retrieved in this way, items are delimited by semicolons (;) instead.
 * 
 * @author Team 2: Michael Camara, Cathal Chaffee, Ayodele Hamilton
 *
 */
public class Input {

    // Retrieve input from an external file named "input.txt"
    static ArrayList<String> importFile() {

        // Indicate file from which list of items will be obtained
        File importFile = new File("input.txt");

        // Create a list to contain initial items 
        ArrayList<String> initialItems = new ArrayList<String>();

        // Obtain input from input.txt text file
        try {
            Scanner scan = new Scanner(importFile);

            // Scan input.txt line-by-line and add to initialItems
            // NOTE: Each line is considered its own String object
            while(scan.hasNext()) {
                String item = scan.nextLine();
                initialItems.add(item);
            }

            scan.close();
        }

        // Throw error if the importFile is not found in the current directory
        catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Cannot find the " + importFile + " file in this directory");
            return null;
        }

        return initialItems;

    }

    // Retrieve input through command line arguments from the console
    static ArrayList<String> importText(String itemList) {

        // Create a list to contain initial items 
        ArrayList<String> initialItems = new ArrayList<String>();

        // Obtain list of items by scanning the text from the console
        Scanner scan = new Scanner(itemList);
        scan.useDelimiter(";");

        while(scan.hasNext()) {
            String currentItem = scan.next();
            initialItems.add(currentItem);
        }

        return initialItems;

    }
}
