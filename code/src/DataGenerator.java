import java.util.ArrayList;
import java.util.List;

/**
 * The DataGenerator class begins the program execution.  It includes importing a list of items from either
 * the console or from "input.txt" in the current directory (via Input), generating all possible permutations
 * of that list using an algorithm selected by the customer (via Sorter), and sending output to the console and
 * to "output.txt" in the current directory (via Output).
 * 
 * @author Team 2: Michael Camara, Cathal Chaffee, Ayodele Hamilton
 */
public class DataGenerator {

    public static void main(String[] args) {

        // Create an initially empty list of items
        ArrayList<String> initialItems = new ArrayList<String>();

        // If user has not entered any arguments in console, import from "input.txt"
        if(args.length == 0) {
            initialItems = Input.importFile();
        }
        // If user has entered arguments in the console, use those arguments as input instead of "input.txt".
        // NOTE!: The following format is required from the /code/ directory:
        // java -cp bin DataGenerator "item1;item2;item3"
        else if(args.length == 1) {
            initialItems = Input.importText(args[0]);
        }

        // If user has entered more than one argument, explain error and stop program execution 
        else {
            System.out.println("Please enclose entire list of items in quotations (\"\") and try again");
            System.out.println("For example: java -cp bin DataGenerator \"item1;item2;item3\"");
            return;
        }

        // Create sorter for generating permutations
        Sorter sorter = new Sorter();

        // Use sorter to generate list of possible permutations
        ArrayList<ArrayList<String>> permutationList = sorter.sort(initialItems);

        // Output this list of permutations to the console
        Output.exportText(permutationList);

        // Also output this list to an external file named "output.txt" in the /code/ directory
        Output.exportFile(permutationList);
    }
}
